FROM golang:latest AS build
WORKDIR /var/tmp
COPY ./source /var/tmp
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux go build -o /var/build/server /var/tmp/cmd/main.go

FROM alpine:latest AS deploy
WORKDIR /var/app
RUN apk --no-cache add ca-certificates tzdata
COPY --from=build /var/build /var/app
EXPOSE 3030
CMD [ "/var/app/server" ]