FROM mysql:latest AS deploy
COPY ./deploy/database.cnf /etc/my.cnf
COPY ../source/model.sql /docker-entrypoint-initdb.d/
RUN chmod 755 /docker-entrypoint-initdb.d/model.sql
RUN chmod 1770 /var/run/mysqld
EXPOSE 1000
CMD [ "mysqld" ]