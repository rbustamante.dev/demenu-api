package database

import (
	"database/sql"
	"fmt"
	"server/logger"
	"strings"
)

type ORM struct {
	DB    *sql.DB
	Query Query
}

type Where struct {
	Field string
	Value interface{}
}

type Query struct {
	Table      string
	Columns    []string
	Values     []interface{}
	Conditions []Where
}

type Selector interface {
	Table(string) Selector
	Columns(...string) Selector
	Values(...interface{}) Selector
	Where(string, interface{}) Selector
	Select() (*sql.Rows, error)
	Insert() (sql.Result, error)
	Update() (sql.Result, error)
	Delete() (sql.Result, error)
}

func NewORM(db *sql.DB) ORM {
	return ORM{
		DB:    db,
		Query: Query{},
	}
}

func (o *ORM) Table(t string) Selector {
	o.Query.Table = t
	return o
}

func (o *ORM) Columns(c ...string) Selector {
	o.Query.Columns = c
	return o
}

func (o *ORM) Values(v ...interface{}) Selector {
	o.Query.Values = v
	return o
}

func (o *ORM) Where(f string, v interface{}) Selector {
	o.Query.Conditions = append(o.Query.Conditions, Where{Field: f, Value: v})
	return o
}

func (o *ORM) iCleanSlices() {
	o.Query.Values = nil
	o.Query.Conditions = nil
}

func (o *ORM) iConditionalQueryParser(query string) (string, []interface{}) {
	defer o.iCleanSlices()
	if len(o.Query.Conditions) > 0 {
		conditions := make([]string, len(o.Query.Conditions))
		values := make([]interface{}, len(o.Query.Conditions))
		for i, con := range o.Query.Conditions {
			conditions[i] = fmt.Sprintf("%s = ?", con.Field)
			values[i] = con.Value
		}
		query += fmt.Sprintf(" WHERE %s", strings.Join(conditions, " AND "))
		o.Query.Values = append(o.Query.Values, values...)
	}
	return query, o.Query.Values
}

func (o *ORM) Select() (*sql.Rows, error) {
	log := logger.GetLogger()
	prequery := fmt.Sprintf("SELECT %s FROM %s", strings.Join(o.Query.Columns, ", "), o.Query.Table)
	query, values := o.iConditionalQueryParser(prequery)
	log.Info("ORM - Select - Query: %v", query)
	log.Info("ORM - Select - Values: %v", values)
	row, err := o.DB.Query(query, values...)
	if err != nil {
		log.Warn("ORM - Select: %v", err)
		return nil, err
	}
	return row, nil
}

func (o *ORM) Insert() (sql.Result, error) {
	log := logger.GetLogger()
	prequery := fmt.Sprintf("INSERT INTO %v (%v) VALUES (?%v)", o.Query.Table, strings.Join(o.Query.Columns, ", "), strings.Repeat(", ?", len(o.Query.Values)-1))
	query, values := o.iConditionalQueryParser(prequery)
	log.Info("ORM - Insert - Query: %v", query)
	log.Info("ORM - Insert - Values: %v", values)
	res, err := o.DB.Exec(query, values...)
	if err != nil {
		log.Warn("ORM - Insert: %v", err)
		return nil, err
	}
	return res, nil
}

func (o *ORM) Update() (sql.Result, error) {
	log := logger.GetLogger()
	for i, col := range o.Query.Columns {
		o.Query.Columns[i] = fmt.Sprintf("%v = ?", col)
	}
	prequery := fmt.Sprintf("UPDATE %v SET %v", o.Query.Table, strings.Join(o.Query.Columns, ", "))
	query, values := o.iConditionalQueryParser(prequery)
	log.Info("ORM - Update - Query: %v", query)
	log.Info("ORM - Update - Values: %v", values)
	res, err := o.DB.Exec(query, values...)
	if err != nil {
		log.Warn("ORM - Update: %v", err)
		return nil, err
	}
	return res, nil
}

func (o *ORM) Delete() (sql.Result, error) {
	log := logger.GetLogger()
	prequery := fmt.Sprintf("DELETE FROM %v", o.Query.Table)
	query, values := o.iConditionalQueryParser(prequery)
	log.Info("ORM - Delete - Query: %v", query)
	log.Info("ORM - Delete - Values: %v", values)
	res, err := o.DB.Exec(query, values...)
	if err != nil {
		log.Warn("ORM - Delete: %v", err)
		return nil, err
	}
	return res, nil
}
