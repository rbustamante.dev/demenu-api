package database

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"server/helper"
	"server/logger"
	"time"
)

func NewDatabase() *sql.DB {
	user := helper.GetEnv("SERVER_DATABASE_USER")
	pass := helper.GetEnv("SERVER_DATABASE_PASS")
	host := helper.GetEnv("SERVER_DATABASE_HOST")
	port := helper.GetEnv("SERVER_DATABASE_PORT")
	name := helper.GetEnv("SERVER_DATABASE_NAME")
	payload := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", user, pass, host, port, name)
	database, err := sql.Open("mysql", payload)
	if err != nil {
		log := logger.GetLogger()
		log.Warn("Error al iniciar la base de datos: %s", err)
		database.Close()
	}
	database.SetConnMaxLifetime(time.Minute * 5)
	return database
}
