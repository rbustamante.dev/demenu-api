package auth

import (
	"github.com/golang-jwt/jwt/v5"
	"server/helper"
	"time"
)

var key = []byte(helper.GetEnv("SERVER_CORE_AUTH"))

func CreateToken(s string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["exp"] = time.Now().Add(50 * time.Hour).Unix()
	claims["sub"] = s
	claims["iat"] = time.Now().Unix()
	tokenString, err := token.SignedString(key)
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func iParser(t *jwt.Token) (interface{}, error) {
	return key, nil
}

func ValidateToken(t string) bool {
	token, err := jwt.Parse(t, iParser)
	if err != nil || !token.Valid {
		return false
	}
	return true
}
