package helper

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/smtp"
	"os"
	"strconv"
	"strings"
	"time"
)

// Lee el cuerpo de la solicitud
func ReadBody(r *http.Request, request interface{}) {
	decoder := json.NewDecoder(r.Body)
	err := decoder.Decode(request)
	if err != nil {
		return
	}
}

// Escribe el cuerpo de la respuesta
func WriteBody(w http.ResponseWriter, response interface{}) {
	w.Header().Add("Content-Type", "application/json")
	encoder := json.NewEncoder(w)
	err := encoder.Encode(response)
	if err != nil {
		return
	}
}

// Establece una cookie personalizada
func SetCookie(w http.ResponseWriter, n string, t time.Duration, v interface{}) {
	if v != nil {
		expiration := time.Now().Add(t * time.Hour)
		cookie := http.Cookie{
			Name:    n,
			Value:   fmt.Sprintf("%v", v),
			Expires: expiration,
			Path:    "/",
		}
		http.SetCookie(w, &cookie)
	}
}

// Obtiene el valor de los parametros en string
func GetStringParam(r *http.Request, p string) string {
	return r.URL.Query().Get(p)
}

// Obtiene el valor de los parametros en int64
func GetInt64Param(r *http.Request, p string) int64 {
	id, err := strconv.ParseInt(r.URL.Query().Get(p), 10, 64)
	if err != nil {
		return 0
	}
	return id
}

// Obtiene la variable de entorno
func GetEnv(e string) string {
	return os.Getenv(e)
}

// Envia un email utilizando el servidor SMTP
func SendEmail(t string, s string, b string) error {
	server := GetEnv("SERVER_SMTP_HOST")
	port := GetEnv("SERVER_SMTP_PORT")
	user := GetEnv("SERVER_SMTP_USER")
	pass := GetEnv("SERVER_SMTP_PASS")
	to := []string{t}
	message := []byte("To: " + strings.Join(to, ",") + "\r\n" + "Subject: " + s + "\r\n" + "\r\n" + b + "\r\n")
	auth := smtp.PlainAuth("server", user, pass, server)
	err := smtp.SendMail(server+":"+port, auth, user, to, message)
	return err
}
