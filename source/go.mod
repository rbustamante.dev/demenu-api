module server

go 1.22.0

require (
	github.com/go-sql-driver/mysql v1.7.1
	github.com/golang-jwt/jwt/v5 v5.2.0
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
)
