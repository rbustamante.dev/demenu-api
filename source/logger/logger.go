package logger

import (
	"log"
	"os"
	"server/helper"
	"sync"
)

type Logger struct {
	Log *log.Logger
}

var (
	logger *Logger
	once   sync.Once
)

func iOpenFile(file string) (*os.File, error) {
	return os.OpenFile(file, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
}

// Obtiene el archivo de registro
func GetLogger() *Logger {
	once.Do(func() {
		file, err := iOpenFile(helper.GetEnv("SERVER_CORE_LOG"))
		if err != nil {
			log.Fatalf("Error al abrir el archivo de registro: %v", err)
		}
		logger = &Logger{
			Log: log.New(file, "", log.Ldate|log.Ltime),
		}
	})
	return logger
}

func (l *Logger) Info(m string, v ...interface{}) {
	if len(v) > 0 {
		l.Log.Printf("[INFO]: "+m+"\n", v...)
	} else {
		l.Log.Printf("[INFO]: " + m + "\n")
	}
}

func (l *Logger) Warn(m string, v ...interface{}) {
	if len(v) > 0 {
		l.Log.Printf("[WARN]: "+m+"\n", v...)
	} else {
		l.Log.Printf("[WARN]: " + m + "\n")
	}
}
