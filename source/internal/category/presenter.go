package category

import (
	"net/http"
	"server/helper"
)

func NewPresenter(i *Interactor) *Presenter {
	return &Presenter{
		Interactor: i,
	}
}

func (p *Presenter) ReadAll(w http.ResponseWriter, r *http.Request) {
	category := Category{}
	payload, message, status := p.Interactor.ReadAll(category)
	helper.WriteBody(w, Response{
		Payload: payload,
		Message: message,
		Status:  status,
	})
}

func (p *Presenter) Read(w http.ResponseWriter, r *http.Request) {
	category := Category{}
	payload, message, status := p.Interactor.Read(category)
	helper.WriteBody(w, Response{
		Payload: payload,
		Message: message,
		Status:  status,
	})
}

func (p *Presenter) Create(w http.ResponseWriter, r *http.Request) {
	category := Category{}
	helper.ReadBody(r, &category)
	message, status := p.Interactor.Create(category)
	helper.WriteBody(w, Response{
		Message: message,
		Status:  status,
	})
}

func (p *Presenter) Update(w http.ResponseWriter, r *http.Request) {
	category := Category{}
	helper.ReadBody(r, &category)
	message, status := p.Interactor.Update(category)
	helper.WriteBody(w, Response{
		Message: message,
		Status:  status,
	})
}

func (p *Presenter) Delete(w http.ResponseWriter, r *http.Request) {
	category := Category{}
	helper.ReadBody(r, &category)
	message, status := p.Interactor.Delete(category)
	helper.WriteBody(w, Response{
		Message: message,
		Status:  status,
	})
}
