package category

import "server/database"

func NewInteractor(o database.ORM) *Interactor {
	return &Interactor{
		ORM: o,
	}
}

func (i *Interactor) ReadAll(c Category) (interface{}, string, bool) {
	var payload []Category
	crow, err := i.ORM.Table("category").Columns("*").Where("pid", c.PID).Select()
	defer crow.Close()
	if err != nil {
		return nil, "Algo salio mal al obtener las categorias.", false
	}
	for crow.Next() {
		err := crow.Scan(&c.ID, &c.PID, &c.Name, &c.Status)
		if err != nil {
			return nil, "Algo salió mal al escanear las categorías, inténtelo nuevamente.", false
		}
		prow, err := i.ORM.Table("product").Columns("id", "pid", "cid", "name", "status", "description", "outstanding", "price", "discount", "image", "size", "vegan", "gluten", "spice", "calories", "delay").Where("cid", c.ID).Select()
		defer prow.Close()
		if err != nil {
			return nil, "Algo salió mal al obtener los productos, inténtelo nuevamente.", false
		}
		var products []Product
		for prow.Next() {
			p := Product{}
			err := prow.Scan(&p.ID, &p.PID, &p.CID, &p.Name, &p.Status, &p.Description, &p.Outstanding, &p.Price, &p.Discount, &p.Image, &p.Size, &p.Vegan, &p.Gluten, &p.Spicy, &p.Calories, &p.Delay)
			if err != nil {
				return nil, "Algo salió mal al obtener los productos, inténtelo nuevamente.", false
			}
			products = append(products, p)
		}
		c.Products = products
		payload = append(payload, c)
	}
	if len(payload) == 0 {
		return nil, "No se encontraron categorías para el usuario.", false
	}
	return payload, "Exito al obtener las categorias.", true
}

func (i *Interactor) Read(c Category) (interface{}, string, bool) {
	crow, err := i.ORM.Table("category").Columns("id", "pid", "name", "status").Where("id", c.ID).Select()
	defer crow.Close()
	if err != nil {
		return nil, "Algo salio mal al obtener la categoria.", false
	}
	if !crow.Next() {
		return nil, "No se encontró ningúna categoria con el ID proporcionado.", false
	} else {
		err := crow.Scan(&c.ID, &c.PID, &c.Name, &c.Status)
		if err != nil {
			return nil, "Algo salio mal al obtener la categoria.", false
		}
		prow, err := i.ORM.Table("product").Columns("id", "pid", "cid", "name", "status", "description", "outstanding", "price", "discount", "image", "size", "vegan", "gluten", "spice", "calories", "delay").Where("cid", c.ID).Select()
		defer prow.Close()
		if err != nil {
			return nil, "Algo salio mal al obtener los productos de la categoria.", false
		}
		var products []Product
		for prow.Next() {
			p := Product{}
			err := prow.Scan(&p.ID, &p.PID, &p.CID, &p.Name, &p.Status, &p.Description, &p.Outstanding, &p.Price, &p.Discount, &p.Image, &p.Size, &p.Vegan, &p.Gluten, &p.Spicy, &p.Calories, &p.Delay)
			if err != nil {
				return nil, "Algo salio mal al obtener los productos de la categoria.", false
			}
			products = append(products, p)
		}
		c.Products = products
	}
	return c, "Exito al obtener la categoria.", true
}

func (i *Interactor) Create(c Category) (string, bool) {
	res, err := i.ORM.Table("category").Columns("pid", "name", "status").Values(c.PID, c.Name, c.Status).Insert()
	if err != nil {
		return "Algo salio mal al crear la categoria.", false
	}
	aff, err := res.RowsAffected()
	if err != nil {
		return "Algo salio mal al crear la categoria.", false
	}
	if aff == 0 {
		return "Error al crear la categoria.", false
	}
	return "Exito al crear la categoria.", true
}

func (i *Interactor) Update(c Category) (string, bool) {
	res, err := i.ORM.Table("category").Columns("pid", "name", "status").Values(c.PID, c.Name, c.Status).Where("id", c.ID).Update()
	if err != nil {
		return "Algo salio mal al actualizar la categoria.", false
	}
	aff, err := res.RowsAffected()
	if err != nil {
		return "Algo salio mal al actualizar la categoria.", false
	}
	if aff == 0 {
		return "Error al actualizar la categoria.", false
	}
	return "Exito al actualizar la categoria.", true
}

func (i *Interactor) Delete(c Category) (string, bool) {
	res, err := i.ORM.Table("category").Where("id", c.ID).Delete()
	if err != nil {
		return "Algo salio mal al eliminar la categoria.", false
	}
	aff, err := res.RowsAffected()
	if err != nil {
		return "Algo salio mal al eliminar la categoria.", false
	}
	if aff == 0 {
		return "Error al eliminar la categoria.", false
	}
	return "Exito al eliminar la categoria.", true
}
