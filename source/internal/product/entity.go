package product

import "server/database"

type Interactor struct {
	ORM database.ORM
}

type Presenter struct {
	Interactor *Interactor
}

type Product struct {
	ID          int64   `json:"id"`
	PID         int64   `json:"pid"`
	CID         int64   `json:"cid"`
	Name        string  `json:"name"`
	Status      bool    `json:"status"`
	Description string  `json:"description"`
	Outstanding bool    `json:"outstanding"`
	Price       float64 `json:"price"`
	Discount    string  `json:"discount"`
	Image       []byte  `json:"image"`
	Size        string  `json:"size"`
	Vegan       bool    `json:"vegan"`
	Gluten      bool    `json:"gluten"`
	Spicy       string  `json:"spicy"`
	Calories    int     `json:"calories"`
	Delay       int     `json:"delay"`
}

type Response struct {
	Payload interface{} `json:"payload,omitempty"`
	Message string      `json:"message,omitempty"`
	Status  bool        `json:"status"`
}
