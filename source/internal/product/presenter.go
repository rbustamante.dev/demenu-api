package product

import (
	"net/http"
	"server/helper"
)

func NewPresenter(i *Interactor) *Presenter {
	return &Presenter{
		Interactor: i,
	}
}

func (p *Presenter) ReadAll(w http.ResponseWriter, r *http.Request) {
	product := Product{}
	payload, message, status := p.Interactor.ReadAll(product)
	helper.WriteBody(w, Response{
		Payload: payload,
		Message: message,
		Status:  status,
	})
}

func (p *Presenter) Read(w http.ResponseWriter, r *http.Request) {
	product := Product{}
	payload, message, status := p.Interactor.Read(product)
	helper.WriteBody(w, Response{
		Payload: payload,
		Message: message,
		Status:  status,
	})
}

func (p *Presenter) Create(w http.ResponseWriter, r *http.Request) {
	product := Product{}
	helper.ReadBody(r, &product)
	message, status := p.Interactor.Create(product)
	helper.WriteBody(w, Response{
		Message: message,
		Status:  status,
	})
}

func (p *Presenter) Update(w http.ResponseWriter, r *http.Request) {
	product := Product{}
	helper.ReadBody(r, &product)
	message, status := p.Interactor.Update(product)
	helper.WriteBody(w, Response{
		Message: message,
		Status:  status,
	})
}

func (p *Presenter) Delete(w http.ResponseWriter, r *http.Request) {
	product := Product{}
	helper.ReadBody(r, &product)
	message, status := p.Interactor.Delete(product)
	helper.WriteBody(w, Response{
		Message: message,
		Status:  status,
	})
}
