package product

import "server/database"

func NewInteractor(o database.ORM) *Interactor {
	return &Interactor{
		ORM: o,
	}
}

func (i *Interactor) ReadAll(p Product) (interface{}, string, bool) {
	var payload []Product
	row, err := i.ORM.Table("product").Columns("*").Where("pid", p.PID).Select()
	defer row.Close()
	if err != nil {
		return nil, "Algo salio mal al obtener los productos.", false
	}
	for row.Next() {
		err := row.Scan(&p.ID, &p.PID, &p.CID, &p.Name, &p.Status, &p.Description, &p.Outstanding, &p.Price, &p.Discount, &p.Image, &p.Size, &p.Vegan, &p.Gluten, &p.Spicy, &p.Calories, &p.Delay)
		if err != nil {
			return nil, "Algo salio mal al obtener los productos.", false
		}
		payload = append(payload, p)
	}
	if len(payload) == 0 {
		return nil, "No se encontraron productos con el PID proporcionado.", false
	}
	return payload, "Exito al obtener los productos.", true
}

func (i *Interactor) Read(p Product) (interface{}, string, bool) {
	row, err := i.ORM.Table("product").Columns("id", "pid", "cid", "name", "status", "description", "outstanding", "price", "discount", "image", "size", "vegan", "gluten", "spice", "calories", "delay").Where("id", p.ID).Select()
	defer row.Close()
	if err != nil {
		return nil, "Algo salio mal al obtener el producto.", false
	}
	if !row.Next() {
		return nil, "No se encontró ningún producto con el ID proporcionado.", false
	} else {
		err := row.Scan(&p.ID, &p.PID, &p.CID, &p.Name, &p.Status, &p.Description, &p.Outstanding, &p.Price, &p.Discount, &p.Image, &p.Size, &p.Vegan, &p.Gluten, &p.Spicy, &p.Calories, &p.Delay)
		if err != nil {
			return nil, "Algo salio mal al obtener el producto.", false
		}
	}
	return p, "Exito al obtener el producto.", true
}

func (i *Interactor) Create(p Product) (string, bool) {
	res, err := i.ORM.Table("product").Columns("pid", "cid", "name", "status", "description", "outstanding", "price", "discount", "image", "size", "vegan", "gluten", "spicy", "calories", "delay").Values(p.PID, p.CID, p.Name, p.Status, p.Description, p.Outstanding, p.Price, p.Discount, p.Image, p.Size, p.Vegan, p.Gluten, p.Spicy, p.Calories, p.Delay).Insert()
	if err != nil {
		return "Algo salio mal al crear el producto.", false
	}
	aff, err := res.RowsAffected()
	if err != nil {
		return "Algo salio mal al crear el producto.", false
	}
	if aff == 0 {
		return "Error al crear el producto.", false
	}
	return "Exito al crear el producto.", true
}

func (i *Interactor) Update(p Product) (string, bool) {
	res, err := i.ORM.Table("product").Columns("pid", "cid", "name", "status", "description", "outstanding", "price", "discount", "image", "size", "vegan", "gluten", "spicy", "calories", "delay").Values(p.PID, p.CID, p.Name, p.Status, p.Description, p.Outstanding, p.Price, p.Discount, p.Image, p.Size, p.Vegan, p.Gluten, p.Spicy, p.Calories, p.Delay).Where("id", p.ID).Update()
	if err != nil {
		return "Algo salio mal al actualizar el producto.", false
	}
	aff, err := res.RowsAffected()
	if err != nil {
		return "Algo salio mal al actualizar el producto.", false
	}
	if aff == 0 {
		return "Error al actualizar el producto.", false
	}
	return "Exito al actualizar el producto.", true
}

func (i *Interactor) Delete(p Product) (string, bool) {
	res, err := i.ORM.Table("product").Where("id", p.ID).Delete()
	if err != nil {
		return "Algo salio mal al eliminar el producto.", false
	}
	aff, err := res.RowsAffected()
	if err != nil {
		return "Algo salio mal al eliminar el producto.", false
	}
	if aff == 0 {
		return "Error al eliminar el producto.", false
	}
	return "Exito al eliminar el producto.", true
}
