package user

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"math/rand"
	"net/http"
	"server/auth"
	"server/database"
	"server/helper"
	"time"
)

func NewInteractor(o database.ORM) *Interactor {
	return &Interactor{
		ORM: o,
	}
}

func (i *Interactor) iValidateEmail(e string) bool {
	row, err := i.ORM.Table("user").Columns("id").Where("email", e).Select()
	if err != nil {
		return true
	}
	defer row.Close()
	return row.Next()
}

func (i *Interactor) iGenerateRandomCode() string {
	characters := "123456789"
	code := make([]byte, 6)
	for i := range code {
		code[i] = characters[rand.Intn(len(characters))]
	}
	return string(code)
}

func (i *Interactor) Read(u User) (interface{}, string, string, bool) {
	selectUserRow, selectUserErr := i.ORM.Table("user").Columns("dni", "email", "name", "surname", "phone", "address", "subscription").Where("id", u.ID).Select()
	if selectUserErr != nil {
		return nil, "Error al obtener el usuario.", "User-Read-001", false
	}
	if !selectUserRow.Next() {
		return nil, "Error al obtener el usuario. No se encontro ningun usuario con el ID proporcionado.", "User-Read-002", false
	} else {
		selectUserRowScanErr := selectUserRow.Scan(&u.DNI, &u.Email, &u.Name, &u.Surname, &u.Phone, &u.Address, &u.Subscription)
		if selectUserRowScanErr != nil {
			return nil, "Error al obtener el usuario.", "User-Read-003", false
		}
	}
	return u, "Exito al obtener el usuario.", "User-Read-004", true
}

func (i *Interactor) Update(u User) (string, string, bool) {
	invalid := i.iValidateEmail(u.Email)
	if invalid {
		return "Error al actualizar el usuario. El email proporcionado ya esta en uso por otro usuario.", "User-Update-001", false
	}
	updateUserRes, updateUserErr := i.ORM.Table("user").Columns("dni", "email", "name", "surname", "phone", "address", "subscription").Values(&u.DNI, &u.Email, &u.Name, &u.Surname, &u.Phone, &u.Address, &u.Subscription).Where("id", u.ID).Update()
	if updateUserErr != nil {
		return "Error al actualizar el usuario.", "User-Update-002", false
	}
	updateUserAff, updateUserAffErr := updateUserRes.RowsAffected()
	if updateUserAffErr != nil {
		return "Error al actualizar el usuario.", "User-Update-003", false
	}
	if updateUserAff == 0 {
		return "Error al actualizar el usuario. No se han encontrado datos nuevos para actualizar.", "User-Update-004", false
	}
	return "Exito al actualizar el usuario.", "User-Update-005", true
}

func (i *Interactor) Delete(u User) (string, string, bool) {
	if u.ID == 0 {
		return "Error al eliminar el usuario. Algunos datos necesarios no han sido proporcionados.", "User-Delete-001", false
	}
	deleteUserRes, deleteUserErr := i.ORM.Table("user").Where("id", u.ID).Delete()
	if deleteUserErr != nil {
		return "Error al eliminar el usuario.", "User-Delete-002", false
	}
	deleteUserAff, deleteUserAffErr := deleteUserRes.RowsAffected()
	if deleteUserAffErr != nil {
		return "Error al eliminar el usuario.", "User-Delete-003", false
	}
	if deleteUserAff == 0 {
		return "Error al eliminar el usuario. No se ha proporcionado un identificador valido.", "User-Delete-004", false
	}
	return "Exito al eliminar el usuario.", "User-Delete-005", true
}

func (i *Interactor) Login(l Login) (interface{}, interface{}, string, string, bool) {
	u := User{}
	inPasswordDecode, inPasswordDecodeErr := base64.StdEncoding.DecodeString(l.Password)
	if inPasswordDecodeErr != nil {
		return nil, nil, "Error al iniciar sesion. No se pudo verificar la contraseña.", "User-Login-001", false
	}
	selectUserRow, selectUserErr := i.ORM.Table("user").Columns("id", "password").Where("email", l.Email).Select()
	if selectUserErr != nil {
		return nil, nil, "Error al iniciar sesion.", "User-Login-002", false
	}
	defer selectUserRow.Close()
	if !selectUserRow.Next() {
		return nil, nil, "Error al iniciar sesion. No se encontro ningun usuario con los datos proporcionados.", "User-Login-003", false
	} else {
		selectUserRowScanErr := selectUserRow.Scan(&u.ID, &u.Password)
		if selectUserRowScanErr != nil {
			return nil, nil, "Error al iniciar sesion. No se encontro ningun usuario con los datos proporcionados.", "User-Login-004", false
		}
	}
	dbPasswordDecode, dbPasswordDecodeErr := base64.StdEncoding.DecodeString(u.Password)
	if dbPasswordDecodeErr != nil {
		return nil, nil, "Error al iniciar sesion. No se pudo verificar la contraseña.", "User-Login-005", false
	}
	if string(inPasswordDecode) != string(dbPasswordDecode) {
		return nil, nil, "Error al iniciar sesion. La contraseña o el email no son validos.", "User-Login-006", false
	}
	token, tokenErr := auth.CreateToken(l.Email)
	if tokenErr != nil {
		return nil, nil, "Error al iniciar sesion. No se pudo establecer el token de seguridad.", "User-Login-007", false
	}
	return token, u.ID, "Exito al iniciar sesion.", "User-Login-008", true
}

func (i *Interactor) Register(r Register) (interface{}, interface{}, string, string, bool) {
	u := User{}
	invalid := i.iValidateEmail(r.Email)
	if invalid {
		return nil, nil, "Error al crear el usuario. El email proporcionado ya esta en uso por otro usuario.", "User-Register-001", false
	}
	insertUserRes, insertUserErr := i.ORM.Table("user").Columns("dni", "email", "password", "name", "surname", "phone", "address").Values(r.DNI, r.Email, r.Password, r.Name, r.Surname, r.Phone, r.Address).Insert()
	if insertUserErr != nil {
		return nil, nil, "Error al crear el usuario.", "User-Register-002", false
	}
	id, idErr := insertUserRes.LastInsertId()
	if idErr != nil {
		return nil, nil, "Error al crear el usuario.", "User-Register-003", false
	}
	u.ID = id
	token, tokenErr := auth.CreateToken(r.Email)
	if tokenErr != nil {
		return nil, nil, "Error al crear el usuario. No se pudo establecer el token de seguridad.", "User-Register-004", false
	}
	return token, u.ID, "Exito al crear el usuario.", "User-Register-005", true
}

func (i *Interactor) RecoverRequest(rr RecoverRequest) (string, string, bool) {
	u := User{}
	selectUserRow, selectUserErr := i.ORM.Table("user").Columns("id").Where("email", rr.Email).Select()
	if selectUserErr != nil {
		return "Error al recuperar la cuenta de usuario. No se encontro ninguna cuenta asociada al correo electronico proporcionado.", "User-RecoverRequest-001", false
	}
	defer selectUserRow.Close()
	if !selectUserRow.Next() {
		return "Error al recuperar la cuenta de usuario. No se encontro ninguna cuenta asociada al correo electronico proporcionado.", "User-RecoverRequest-002", false
	} else {
		selectUserRowScanErr := selectUserRow.Scan(&u.ID)
		if selectUserRowScanErr != nil {
			return "Error al recuperar la cuenta de usuario.", "User-RecoverRequest-003", false
		}
	}
	code := i.iGenerateRandomCode()
	ur := UserRecover{
		UID:     u.ID,
		Code:    code,
		Expires: time.Now().Add(time.Hour).Format(time.RFC3339),
	}
	selectUserRecoverRow, selectUserRecoverErr := i.ORM.Table("user_recover").Columns("id").Where("uid", u.ID).Select()
	if selectUserRecoverErr != nil {
		return "Error al recuperar la cuenta de usuario.", "User-RecoverRequest-004", false
	}
	defer selectUserRecoverRow.Close()
	if !selectUserRecoverRow.Next() {
		insertUserRecoverRes, insertUserRecoverErr := i.ORM.Table("user_recover").Columns("uid", "code", "expires").Values(u.ID, ur.Code, ur.Expires).Insert()
		if insertUserRecoverErr != nil {
			return "Error al recuperar la cuenta de usuario.", "User-RecoverRequest-005", false
		}
		insertUserRecoverAff, insertUserRecoverAffErr := insertUserRecoverRes.RowsAffected()
		if insertUserRecoverAffErr != nil {
			return "Error al recuperar la cuenta de usuario.", "User-RecoverRequest-006", false
		}
		if insertUserRecoverAff == 0 {
			return "Error al recuperar la cuenta de usuario. No se han encontrado datos nuevos para actualizar.", "User-RecoverRequest-007", false
		}
	} else {
		selectUserRecoverRowScanErr := selectUserRecoverRow.Scan(&ur.ID)
		if selectUserRecoverRowScanErr != nil {
			return "Error al recuperar la cuenta de usuario.", "User-RecoverRequest-008", false
		}
		updateUserRecoverRes, updateUserRecoverErr := i.ORM.Table("user_recover").Columns("uid", "code", "expires").Values(u.ID, ur.Code, ur.Expires).Where("id", ur.ID).Update()
		if updateUserRecoverErr != nil {
			return "Error al recuperar la cuenta de usuario.", "User-RecoverRequest-009", false
		}
		updateUserRecoverAff, updateUserRecoverAffErr := updateUserRecoverRes.RowsAffected()
		if updateUserRecoverAffErr != nil {
			return "Error al recuperar la cuenta de usuario.", "User-RecoverValidate-010", false
		}
		if updateUserRecoverAff == 0 {
			return "Error al recuperar la cuenta de usuario. No se han encontrado datos nuevos para actualizar.", "User-RecoverValidate-011", false
		}
	}
	body := "Codigo: " + ur.Code
	sendRecoverEmailErr := helper.SendEmail(rr.Email, "Codigo", body)
	if sendRecoverEmailErr != nil {
		return "Error al recuperar la cuenta de usuario. No se pudo enviar el codigo por correo electronico.", "User-RecoverRequest-012", false
	}
	return "Se ha enviado con exito el codigo de recuperacion por correo electronico.", "User-RecoverRequest-013", true
}

func (i *Interactor) RecoverValidate(rv RecoverValidate) (string, string, bool) {
	u := User{}
	selectUserRow, selectUserErr := i.ORM.Table("user").Columns("id", "password").Where("email", rv.Email).Select()
	if selectUserErr != nil {
		return "Error al recuperar la cuenta de usuario. No se encontro ninguna cuenta asociada al correo electronico proporcionado.", "User-RecoverValidate-001", false
	}
	defer selectUserRow.Close()
	if !selectUserRow.Next() {
		return "Error al recuperar la cuenta de usuario. No se encontro ninguna cuenta asociada al correo electronico proporcionado.", "User-RecoverValidate-002", false
	} else {
		selectUserRowScanErr := selectUserRow.Scan(&u.ID, &u.Password)
		if selectUserRowScanErr != nil {
			return "Error al recuperar la cuenta de usuario.", "User-RecoverValidate-003", false
		}
	}
	selectUserRecoverRow, selectUserRecoverErr := i.ORM.Table("user_recover").Columns("id", "uid", "code", "expires").Where("uid", u.ID).Select()
	if selectUserRecoverErr != nil {
		return "Error al recuperar la cuenta de usuario.", "User-RecoverValidate-004", false
	}
	defer selectUserRecoverRow.Close()
	ur := UserRecover{}
	if !selectUserRecoverRow.Next() {
		return "Error al recuperar la cuenta de usuario. No se encontro ningun codigo de recuperacion asociado a esta cuenta.", "User-RecoverValidate-005", false
	} else {
		selectUserRecoverRowScanErr := selectUserRecoverRow.Scan(&ur.ID, &ur.UID, &ur.Code, &ur.Expires)
		if selectUserRecoverRowScanErr != nil {
			return "Error al recuperar la cuenta de usuario.", "User-RecoverValidate-006", false
		}
	}
	expires, expiresErr := time.Parse(time.RFC3339, ur.Expires)
	if expiresErr != nil {
		return "Error al recuperar la cuenta de usuario.", "User-RecoverValidate-007", false
	}
	if expires.Before(time.Now()) {
		return "Error al recuperar la cuenta de usuario. El codigo de recuperacion ha expirado.", "User-RecoverValidate-008", false
	}
	if ur.Code != rv.Code {
		return "Error al recuperar la cuenta de usuario. El codigo de recuperacion proporcionado es incorrecto.", "User-RecoverValidate-009", false
	}
	if u.Password != rv.Password {
		updateUserRes, updateUserErr := i.ORM.Table("user").Columns("password").Values(rv.Password).Where("id", u.ID).Update()
		if updateUserErr != nil {
			return "Error al recuperar la cuenta de usuario.", "User-RecoverValidate-010", false
		}
		updateUserAff, updateUserAffErr := updateUserRes.RowsAffected()
		if updateUserAffErr != nil {
			return "Error al recuperar la cuenta de usuario.", "User-RecoverValidate-011", false
		}
		if updateUserAff == 0 {
			return "Error al recuperar la cuenta de usuario. No se han encontrado datos nuevos para actualizar.", "User-RecoverValidate-012", false
		}
	}
	return "Se ha recuperado con exito la cuenta de usuario.", "User-RecoverValidate-013", true
}

func (i *Interactor) SubscriptionRead(sr SubscriptionRead) (interface{}, string, string, bool) {
	if sr.ID == "" {
		return nil, "Error al obtener la subscripcion. Algunos datos necesarios no han sido proporcionados.", "User-SubscriptionRead-001", false
	}
	url := helper.GetEnv("SERVER_SUBSCRIPTION_HOST") + "/preapproval/" + sr.ID
	readSubscriptionReq, readSubscriptionErr := http.NewRequest("GET", url, nil)
	if readSubscriptionErr != nil {
		return nil, "Error al obtener la subscripcion.", "User-SubscriptionRead-002", false
	}
	token := helper.GetEnv("SERVER_SUBSCRIPTION_AUTH")
	readSubscriptionReq.Header.Set("Authorization", "Bearer "+token)
	client := http.Client{}
	clientRes, clientErr := client.Do(readSubscriptionReq)
	if clientErr != nil {
		return nil, "Error al obtener la subscripcion.", "User-SubscriptionRead-003", false
	}
	defer clientRes.Body.Close()
	if clientRes.StatusCode != http.StatusOK {
		return nil, "Error al obtener la subscripcion. Verifique los datos ingresados e intente nuevamente.", "User-SubscriptionRead-004", false
	}
	clientResDecodeErr := json.NewDecoder(clientRes.Body).Decode(&sr)
	if clientResDecodeErr != nil {
		return nil, "Error al obtener la subscripcion.", "User-SubscriptionRead-005", false
	}
	return sr, "Exito al obtener la subscripcion.", "User-SubscriptionRead-006", true
}

func (i *Interactor) SubscriptionCreate(sc SubscriptionCreate) (string, string, bool) {
	if sc.UID == 0 {
		return "Error al crear la subscripcion. Algunos datos necesarios no han sido proporcionados.", "User-SubscriptionCreate-001", false
	}
	url := helper.GetEnv("SERVER_SUBSCRIPTION_HOST") + "/preapproval"
	fields := iMercadoPagoCreateRequest{
		BackUrl:           sc.BackUrl,
		CardTokenID:       sc.CardTokenID,
		PayerEmail:        sc.PayerEmail,
		PreapprovalPlanID: sc.PreapprovalPlanID,
	}
	fieldsByte, fieldsByteErr := json.Marshal(fields)
	if fieldsByteErr != nil {
		return "Error al crear la subscripcion.", "User-SubscriptionCreate-002", false
	}
	payload := bytes.NewReader(fieldsByte)
	createSubscriptionReq, createSubscriptionErr := http.NewRequest("POST", url, payload)
	if createSubscriptionErr != nil {
		return "Error al crear la subscripcion.", "User-SubscriptionCreate-003", false
	}
	token := helper.GetEnv("SERVER_SUBSCRIPTION_AUTH")
	createSubscriptionReq.Header.Set("Content-Type", "application/json")
	createSubscriptionReq.Header.Set("Authorization", "Bearer "+token)
	client := http.Client{}
	clientRes, clientErr := client.Do(createSubscriptionReq)
	if clientErr != nil {
		return "Error al crear la subscripcion.", "User-SubscriptionCreate-004", false
	}
	defer clientRes.Body.Close()
	if clientRes.StatusCode != http.StatusCreated {
		return "Error al crear la subscripcion. Verifique los datos ingresados e intente nuevamente.", "User-SubscriptionCreate-005", false
	}
	mercadoPagoCreateResponse := iMercadoPagoCreateResponse{}
	clientResDecodeErr := json.NewDecoder(clientRes.Body).Decode(&mercadoPagoCreateResponse)
	if clientResDecodeErr != nil {
		return "Error al crear la subscripcion.", "User-SubscriptionCreate-006", false
	}
	if mercadoPagoCreateResponse.Status != "authorized" {
		return "Error al crear la subscripcion. El pago no fue autorizado, intente nuevamente.", "User-SubscriptionCreate-007", false
	}
	updateUserRes, updateUserErr := i.ORM.Table("user").Columns("subscription").Values(mercadoPagoCreateResponse.ID).Where("id", sc.UID).Update()
	if updateUserErr != nil {
		return "Error al crear la subscripcion.", "User-SubscriptionCreate-008", false
	}
	updateUserAff, updateUserAffErr := updateUserRes.RowsAffected()
	if updateUserAffErr != nil {
		return "Error al crear la subscripcion.", "User-SubscriptionCreate-009", false
	}
	if updateUserAff == 0 {
		return "Error al crear la subscripcion. No se han encontrado datos nuevos para actualizar.", "User-SubscriptionCreate-010", false
	}
	return "Exito al crear la subscripcion.", "User-SubscriptionCreate-011", true
}

func (i *Interactor) SubscriptionUpdate(su SubscriptionUpdate) (string, string, bool) {
	if su.ID == "" {
		return "Error al cancelar la subscripcion. Algunos datos necesarios no han sido proporcionados.", "User-SubscriptionUpdate-001", false
	}
	url := helper.GetEnv("SERVER_SUBSCRIPTION_HOST") + "/preapproval/" + su.ID
	fields := iMercadoPagoUpdateRequest{
		Status: "cancelled",
	}
	fieldsByte, fieldsByteErr := json.Marshal(fields)
	if fieldsByteErr != nil {
		return "Error al cancelar la subscripcion.", "User-SubscriptionUpdate-002", false
	}
	payload := bytes.NewReader(fieldsByte)
	updateSubscriptionReq, updateSubscriptionErr := http.NewRequest("PUT", url, payload)
	if updateSubscriptionErr != nil {
		return "Error al cancelar la subscripcion.", "User-SubscriptionUpdate--003", false
	}
	token := helper.GetEnv("SERVER_SUBSCRIPTION_AUTH")
	updateSubscriptionReq.Header.Set("Content-Type", "application/json")
	updateSubscriptionReq.Header.Set("Authorization", "Bearer "+token)
	client := http.Client{}
	clientRes, clientErr := client.Do(updateSubscriptionReq)
	if clientErr != nil {
		return "Error al cancelar la subscripcion.", "User-SubscriptionUpdate-004", false
	}
	defer clientRes.Body.Close()
	if clientRes.StatusCode != http.StatusOK {
		return "Error al cancelar la subscripcion. Verifique los datos ingresados e intente nuevamente.", "User-SubscriptionUpdate-005", false
	}
	mercadoPagoUpdateResponse := iMercadoPagoUpdateResponse{}
	clientResDecodeErr := json.NewDecoder(clientRes.Body).Decode(&mercadoPagoUpdateResponse)
	if clientResDecodeErr != nil {
		return "Error al cancelar la subscripcion.", "User-SubscriptionUpdate-006", false
	}
	if mercadoPagoUpdateResponse.Status != "cancelled" {
		return "Error al cancelar la subscripcion. No pudimos procesar la solicitud, intente nuevamente.", "User-SubscriptionUpdate-007", false
	}
	return "Exito al cancelar la subscripcion.", "User-SubscriptionUpdate-008", true
}
