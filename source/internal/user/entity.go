package user

import (
	"server/database"
)

type Interactor struct {
	ORM database.ORM
}

type Presenter struct {
	Interactor *Interactor
}

type User struct {
	ID           int64  `json:"id"`
	DNI          int    `json:"dni"`
	Email        string `json:"email"`
	Password     string `json:"password,omitempty"`
	Name         string `json:"name"`
	Surname      string `json:"surname"`
	Phone        int    `json:"phone"`
	Address      string `json:"address"`
	Subscription string `json:"subscription"`
}

type Register struct {
	DNI      int    `json:"dni"`
	Email    string `json:"email"`
	Password string `json:"password"`
	Name     string `json:"name"`
	Surname  string `json:"surname"`
	Phone    int    `json:"phone"`
	Address  string `json:"address"`
}

type Login struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type UserRecover struct {
	ID      int64  `json:"id"`
	UID     int64  `json:"uid"`
	Code    string `json:"code"`
	Expires string `json:"expires"`
}

type RecoverRequest struct {
	Email string `json:"email"`
}

type RecoverValidate struct {
	Email    string `json:"email"`
	Password string `json:"password"`
	Code     string `json:"code"`
}

type SubscriptionRead struct {
	ID                string `json:"id"`
	LastModified      string `json:"last_modified"`
	NextPaymentDate   string `json:"next_payment_date"`
	PreapprovalPlanID string `json:"preapproval_plan_id"`
	Status            string `json:"status"`
}
type SubscriptionCreate struct {
	UID               int64  `json:"uid"`
	BackUrl           string `json:"back_url"`
	CardTokenID       string `json:"card_token_id"`
	PayerEmail        string `json:"payer_email"`
	PreapprovalPlanID string `json:"preapproval_plan_id"`
}

type iMercadoPagoCreateRequest struct {
	BackUrl           string `json:"back_url"`
	CardTokenID       string `json:"card_token_id"`
	PayerEmail        string `json:"payer_email"`
	PreapprovalPlanID string `json:"preapproval_plan_id"`
}
type iMercadoPagoCreateResponse struct {
	ID                string `json:"id"`
	PreapprovalPlanID string `json:"preapproval_plan_id"`
	Status            string `json:"status"`
}

type SubscriptionUpdate struct {
	ID string `json:"id"`
}

type iMercadoPagoUpdateRequest struct {
	Status string `json:"status"`
}

type iMercadoPagoUpdateResponse struct {
	Status string `json:"status"`
}

type Response struct {
	Payload interface{} `json:"payload,omitempty"`
	Message string      `json:"message"`
	Code    string      `json:"code"`
	Status  bool        `json:"status"`
}
