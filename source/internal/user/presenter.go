package user

import (
	"net/http"
	"server/helper"
)

func NewPresenter(i *Interactor) *Presenter {
	return &Presenter{
		Interactor: i,
	}
}

func (p *Presenter) Read(w http.ResponseWriter, r *http.Request) {
	user := User{
		ID: helper.GetInt64Param(r, "id"),
	}
	payload, message, code, status := p.Interactor.Read(user)
	helper.WriteBody(w, Response{
		Payload: payload,
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) Update(w http.ResponseWriter, r *http.Request) {
	user := User{
		ID: helper.GetInt64Param(r, "id"),
	}
	helper.ReadBody(r, &user)
	message, code, status := p.Interactor.Update(user)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) Delete(w http.ResponseWriter, r *http.Request) {
	user := User{
		ID: helper.GetInt64Param(r, "id"),
	}
	message, code, status := p.Interactor.Delete(user)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) Login(w http.ResponseWriter, r *http.Request) {
	login := Login{}
	helper.ReadBody(r, &login)
	token, payload, message, code, status := p.Interactor.Login(login)
	helper.SetCookie(w, "Auth-Cookie", 48, token)
	helper.SetCookie(w, "User-Cookie", 48, payload)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) Register(w http.ResponseWriter, r *http.Request) {
	register := Register{}
	helper.ReadBody(r, &register)
	token, payload, message, code, status := p.Interactor.Register(register)
	helper.SetCookie(w, "Auth-Cookie", 48, token)
	helper.SetCookie(w, "User-Cookie", 48, payload)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) RecoverRequest(w http.ResponseWriter, r *http.Request) {
	recoverRequest := RecoverRequest{}
	helper.ReadBody(r, &recoverRequest)
	message, code, status := p.Interactor.RecoverRequest(recoverRequest)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) RecoverValidate(w http.ResponseWriter, r *http.Request) {
	recoverValidate := RecoverValidate{}
	helper.ReadBody(r, &recoverValidate)
	message, code, status := p.Interactor.RecoverValidate(recoverValidate)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) SubscriptionRead(w http.ResponseWriter, r *http.Request) {
	subscriptionRead := SubscriptionRead{
		ID: helper.GetStringParam(r, "id"),
	}
	payload, message, code, status := p.Interactor.SubscriptionRead(subscriptionRead)
	helper.WriteBody(w, Response{
		Payload: payload,
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) SubscriptionCreate(w http.ResponseWriter, r *http.Request) {
	subscriptionCreate := SubscriptionCreate{
		PreapprovalPlanID: helper.GetEnv("SERVER_SUBSCRIPTION_PLAN"),
	}
	helper.ReadBody(r, &subscriptionCreate)
	message, code, status := p.Interactor.SubscriptionCreate(subscriptionCreate)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) SubscriptionUpdate(w http.ResponseWriter, r *http.Request) {
	subscriptionUpdate := SubscriptionUpdate{
		ID: helper.GetStringParam(r, "id"),
	}
	message, code, status := p.Interactor.SubscriptionUpdate(subscriptionUpdate)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}
