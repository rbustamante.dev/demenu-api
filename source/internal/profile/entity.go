package profile

import "server/database"

type Interactor struct {
	ORM database.ORM
}

type Presenter struct {
	Interactor *Interactor
}

type Profile struct {
	ID          int64  `json:"id"`
	UID         int64  `json:"uid"`
	Name        string `json:"name"`
	Nickname    string `json:"nickname"`
	Description string `json:"description"`
	Image       []byte `json:"image"`
	ImageHeader []byte `json:"image_header"`
	Color       string `json:"color"`
}

type ProfileCode struct {
	ID    int64  `json:"id"`
	PID   int64  `json:"pid"`
	Url   string `json:"url"`
	Image []byte `json:"image"`
}

type Response struct {
	Payload interface{} `json:"payload,omitempty"`
	Message string      `json:"message"`
	Code    string      `json:"code"`
	Status  bool        `json:"status"`
}
