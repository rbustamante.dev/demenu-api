package profile

import (
	"github.com/skip2/go-qrcode"
	"server/database"
	"server/logger"
)

func NewInteractor(o database.ORM) *Interactor {
	return &Interactor{
		ORM: o,
	}
}

func (i *Interactor) iValidateName(n string) bool {
	row, err := i.ORM.Table("profile").Columns("id").Where("name", n).Select()
	if err != nil {
		return true
	}
	defer row.Close()
	return row.Next()
}

func (i *Interactor) iValidateCode(pid int64) bool {
	row, err := i.ORM.Table("profile_code").Columns("id").Where("pid", pid).Select()
	if err != nil {
		return true
	}
	defer row.Close()
	return row.Next()
}

func (i *Interactor) Read(p Profile) (interface{}, string, string, bool) {
	if p.UID == 0 && p.Name == "" {
		return nil, "Error al obtener el perfil. Algunos datos necesarios no han sido proporcionados.", "Profile-Read-001", false
	}
	selectProfileQuery := i.ORM.Table("profile").Columns("id", "uid", "name", "nickname", "description", "image", "image_header", "color")
	if p.UID != 0 {
		selectProfileQuery = selectProfileQuery.Where("uid", p.UID)
	}
	if p.Name != "" {
		selectProfileQuery = selectProfileQuery.Where("name", p.Name)
	}
	selectProfileRow, selectProfileErr := selectProfileQuery.Select()
	if selectProfileErr != nil {
		return nil, "Error al obtener el perfil.", "Profile-Read-002", false
	}
	defer selectProfileRow.Close()
	if !selectProfileRow.Next() {
		return nil, "Error al obtener el perfil. No se encontro ningun perfil con el identificador proporcionado.", "Profile-Read-003", false
	} else {
		selectProfileRowScanErr := selectProfileRow.Scan(&p.ID, &p.UID, &p.Name, &p.Nickname, &p.Description, &p.Image, &p.ImageHeader, &p.Color)
		if selectProfileRowScanErr != nil {
			return nil, "Error al obtener el perfil.", "Profile-Read-004", false
		}
	}
	return p, "Exito al obtener el perfil.", "Profile-Read-005", true
}

func (i *Interactor) Create(p Profile) (string, string, bool) {
	invalid := i.iValidateName(p.Name)
	if invalid {
		return "Error al crear el perfil. El nombre de perfil ya esta en uso por otro usuario.", "Profile-Create-001", false
	}
	insertProfileRes, insertProfileErr := i.ORM.Table("profile").Columns("uid", "name", "nickname", "description", "image", "image_header", "color").Values(p.UID, p.Name, p.Nickname, p.Description, p.Image, p.ImageHeader, p.Color).Insert()
	if insertProfileErr != nil {
		return "Error al crear el perfil.", "Profile-Create-002", false
	}
	insertProfileAff, insertProfileAffErr := insertProfileRes.RowsAffected()
	if insertProfileAffErr != nil {
		return "Error al crear el perfil.", "Profile-Create-003", false
	}
	if insertProfileAff == 0 {
		return "Error al crear el perfil. Algunos datos necesarios no han sido proporcionados.", "Profile-Create-004", false
	}
	return "Exito al crear el perfil.", "Profile-Create-005", true
}

func (i *Interactor) Update(p Profile) (string, string, bool) {
	if p.ID == 0 {
		return "Error al actualizar el perfil. Algunos datos necesarios no han sido proporcionados.", "Profile-Update-001", false
	}
	invalid := i.iValidateName(p.Name)
	if invalid {
		return "Error al actualizar el perfil. El nombre de perfil ya esta en uso por otro usuario.", "Profile-Update-002", false
	}
	updateProfileRes, updateProfileErr := i.ORM.Table("profile").Columns("name", "nickname", "description", "image", "image_header", "color").Values(p.Name, p.Nickname, p.Description, p.Image, p.ImageHeader, p.Color).Where("id", p.ID).Update()
	if updateProfileErr != nil {
		return "Error al actualizar el perfil.", "Profile-Update-003", false
	}
	updateProfileAff, updateProfileAffErr := updateProfileRes.RowsAffected()
	if updateProfileAffErr != nil {
		return "Error al actualizar el perfil.", "Profile-Update-004", false
	}
	if updateProfileAff == 0 {
		return "Error al actualizar el perfil. No se han encontrado datos nuevos para actualizar.", "Profile-Update-005", false
	}
	return "Exito al actualizar el perfil.", "Profile-Update-006", true
}

func (i *Interactor) Delete(p Profile) (string, string, bool) {
	if p.ID == 0 {
		return "Error al eliminar el perfil. Algunos datos necesarios no han sido proporcionados.", "Profile-Delete-001", false
	}
	deleteProfileRes, deleteProfileErr := i.ORM.Table("profile").Where("id", p.ID).Delete()
	if deleteProfileErr != nil {
		return "Error al eliminar el perfil.", "Profile-Delete-002", false
	}
	deleteProfileAff, deleteProfileAffErr := deleteProfileRes.RowsAffected()
	if deleteProfileAffErr != nil {
		return "Error al eliminar el perfil.", "Profile-Delete-003", false
	}
	if deleteProfileAff == 0 {
		return "Error al eliminar el perfil. No se ha proporcionado un identificador valido.", "Profile-Delete-004", false
	}
	return "Exito al eliminar el perfil.", "Profile-Delete-005", true
}

func (i *Interactor) ReadCode(pc ProfileCode) (interface{}, string, string, bool) {
	if pc.PID == 0 {
		return nil, "Error al obtener el codigo QR. Algunos datos necesarios no han sido proporcionados.", "Profile-ReadCode-001", false
	}
	selectProfileCodeRow, selectProfileCodeErr := i.ORM.Table("profile_code").Columns("id", "pid", "url", "image").Where("pid", pc.PID).Select()
	if selectProfileCodeErr != nil {
		return nil, "Error al obtener el codigo QR.", "Profile-ReadCode-002", false
	}
	defer selectProfileCodeRow.Close()
	if !selectProfileCodeRow.Next() {
		return nil, "Error al obtener el codigo QR. No se encontro ningun perfil con el identificador proporcionado.", "Profile-ReadCode-003", false
	} else {
		selectProfileCodeRowScanErr := selectProfileCodeRow.Scan(&pc.ID, &pc.PID, &pc.Url, &pc.Image)
		if selectProfileCodeRowScanErr != nil {
			return nil, "Error al obtener el codigo QR.", "Profile-ReadCode-004", false
		}
	}
	return pc, "Exito al obtener el codigo QR.", "Profile-ReadCode-005", true
}

func (i *Interactor) CreateCode(pc ProfileCode) (string, string, bool) {
	invalid := i.iValidateCode(pc.PID)
	if invalid {
		return "Error al crear el codigo QR. El perfil ya dispone de un codigo QR.", "Profile-CreateCode-001", false
	}
	log := logger.GetLogger()
	image, imageErr := qrcode.Encode(pc.Url, qrcode.Medium, 256)
	log.Warn("Erro QR %v", imageErr)
	if imageErr != nil {
		return "Error al crear el codigo QR.", "Profile-CreateCode-002", false
	}
	pc.Image = image
	insertProfileCodeRes, insertProfileCodeErr := i.ORM.Table("profile_code").Columns("pid", "url", "image").Values(pc.PID, pc.Url, pc.Image).Insert()
	if insertProfileCodeErr != nil {
		return "Error al crear el codigo QR.", "Profile-CreateCode-003", false
	}
	insertProfileCodeAff, insertProfileCodeAffErr := insertProfileCodeRes.RowsAffected()
	if insertProfileCodeAffErr != nil {
		return "Error al crear el codigo QR.", "Profile-CreateCode-004", false
	}
	if insertProfileCodeAff == 0 {
		return "Error al crear el codigo QR. Algunos datos necesarios no han sido proporcionados.", "Profile-CreateCode-005", false
	}
	return "Exito al crear el codigo QR.", "Profile-CreateCode-006", true
}

func (i *Interactor) UpdateCode(pc ProfileCode) (string, string, bool) {
	if pc.ID == 0 {
		return "Error al actualizar el codigo QR. Algunos datos necesarios no han sido proporcionados.", "Profile-UpdateCode-001", false
	}
	image, imageErr := qrcode.Encode(pc.Url, qrcode.Medium, 256)
	if imageErr != nil {
		return "Error al crear el codigo QR.", "Profile-UpdateCode-002", false
	}
	pc.Image = image
	updateProfileCodeRes, updateProfileCodeErr := i.ORM.Table("profile_code").Columns("pid", "url", "image").Values(pc.PID, pc.Url, pc.Image).Where("id", pc.ID).Update()
	if updateProfileCodeErr != nil {
		return "Error al actualizar el codigo QR.", "Profile-UpdateCode-003", false
	}
	updateProfileAff, updateProfileAffErr := updateProfileCodeRes.RowsAffected()
	if updateProfileAffErr != nil {
		return "Error al actualizar el codigo QR.", "Profile-UpdateCode-004", false
	}
	if updateProfileAff == 0 {
		return "Error al actualizar el codigo QR. No se han encontrado datos nuevos para actualizar.", "Profile-UpdateCode-005", false
	}
	return "Exito al actualizar el codigo QR.", "Profile-UpdateCode-006", true
}

func (i *Interactor) DeleteCode(pc ProfileCode) (string, string, bool) {
	if pc.ID == 0 {
		return "Error al eliminar el codigo QR. Algunos datos necesarios no han sido proporcionados.", "Profile-DeleteCode-001", false
	}
	deleteProfileRes, deleteProfileErr := i.ORM.Table("profile_code").Where("id", pc.ID).Delete()
	if deleteProfileErr != nil {
		return "Error al eliminar el codigo QR.", "Profile-DeleteCode-002", false
	}
	deleteProfileAff, deleteProfileAffErr := deleteProfileRes.RowsAffected()
	if deleteProfileAffErr != nil {
		return "Error al eliminar el codigo QR.", "Profile-DeleteCode-003", false
	}
	if deleteProfileAff == 0 {
		return "Error al eliminar el codigo QR. No se ha proporcionado un identificador valido.", "Profile-DeleteCode-004", false
	}
	return "Exito al eliminar el codigo QR.", "Profile-DeleteCode-005", true
}
