package profile

import (
	"net/http"
	"server/helper"
)

func NewPresenter(i *Interactor) *Presenter {
	return &Presenter{
		Interactor: i,
	}
}

func (p *Presenter) Read(w http.ResponseWriter, r *http.Request) {
	profile := Profile{
		UID:  helper.GetInt64Param(r, "uid"),
		Name: helper.GetStringParam(r, "name"),
	}
	payload, message, code, status := p.Interactor.Read(profile)
	helper.WriteBody(w, Response{
		Payload: payload,
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) Create(w http.ResponseWriter, r *http.Request) {
	profile := Profile{}
	helper.ReadBody(r, &profile)
	message, code, status := p.Interactor.Create(profile)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) Update(w http.ResponseWriter, r *http.Request) {
	profile := Profile{
		ID: helper.GetInt64Param(r, "id"),
	}
	helper.ReadBody(r, &profile)
	message, code, status := p.Interactor.Update(profile)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) Delete(w http.ResponseWriter, r *http.Request) {
	profile := Profile{
		ID: helper.GetInt64Param(r, "id"),
	}
	message, code, status := p.Interactor.Delete(profile)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) ReadCode(w http.ResponseWriter, r *http.Request) {
	profileCode := ProfileCode{
		PID: helper.GetInt64Param(r, "pid"),
	}
	payload, message, code, status := p.Interactor.ReadCode(profileCode)
	helper.WriteBody(w, Response{
		Payload: payload,
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) CreateCode(w http.ResponseWriter, r *http.Request) {
	profileCode := ProfileCode{}
	helper.ReadBody(r, &profileCode)
	message, code, status := p.Interactor.CreateCode(profileCode)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) UpdateCode(w http.ResponseWriter, r *http.Request) {
	profileCode := ProfileCode{
		ID: helper.GetInt64Param(r, "id"),
	}
	helper.ReadBody(r, &profileCode)
	message, code, status := p.Interactor.UpdateCode(profileCode)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}

func (p *Presenter) DeleteCode(w http.ResponseWriter, r *http.Request) {
	profileCode := ProfileCode{
		ID: helper.GetInt64Param(r, "id"),
	}
	message, code, status := p.Interactor.DeleteCode(profileCode)
	helper.WriteBody(w, Response{
		Message: message,
		Code:    code,
		Status:  status,
	})
}
