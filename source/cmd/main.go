package main

import (
	"fmt"
	"net/http"
	"server/database"
	"server/helper"
	"server/internal/category"
	"server/internal/product"
	"server/internal/profile"
	"server/internal/user"
	"server/logger"
	"server/router"
	"time"
)

func main() {
	// Database
	data := database.NewDatabase()
	orm := database.NewORM(data)
	defer data.Close()

	// Internal
	userInteractor := user.NewInteractor(orm)
	userPresenter := user.NewPresenter(userInteractor)
	profileInteractor := profile.NewInteractor(orm)
	profilePresenter := profile.NewPresenter(profileInteractor)
	categoryInteractor := category.NewInteractor(orm)
	categoryPresenter := category.NewPresenter(categoryInteractor)
	productInteractor := product.NewInteractor(orm)
	productPresenter := product.NewPresenter(productInteractor)

	// Router
	router := router.NewRouter()
	router.Resource("/api/user").User(userPresenter)
	router.Resource("/api/profile").Profile(profilePresenter)
	router.Resource("/api/category").Category(categoryPresenter)
	router.Resource("/api/product").Product(productPresenter)

	// Server
	port := helper.GetEnv("SERVER_CORE_PORT")
	server := &http.Server{
		Addr:         fmt.Sprintf(":%s", port),
		Handler:      router,
		ReadTimeout:  time.Second * 10,
		WriteTimeout: time.Second * 10,
	}

	// Logger
	log := logger.GetLogger()
	log.Info("SERVER - LISTEN ON PORT: %s", port)
	if err := server.ListenAndServe(); err != nil {
		log.Warn("Error al iniciar el servidor: %s", err)
	}
}
