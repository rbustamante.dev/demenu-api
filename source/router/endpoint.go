package router

import (
	"server/internal/category"
	"server/internal/product"
	"server/internal/profile"
	"server/internal/user"
)

func (r *Router) User(p *user.Presenter) {
	r.Endpoint("GET", "/read/", p.Read, true)
	r.Endpoint("PUT", "/update/", p.Update, true)
	r.Endpoint("DELETE", "/delete/", p.Delete, true)
	r.Endpoint("POST", "/login/", p.Login, false)
	r.Endpoint("POST", "/register/", p.Register, false)
	r.Endpoint("POST", "/recover/request/", p.RecoverRequest, false)
	r.Endpoint("POST", "/recover/validate/", p.RecoverValidate, false)
	r.Endpoint("GET", "/subscription/read/", p.SubscriptionRead, true)
	r.Endpoint("POST", "/subscription/create/", p.SubscriptionCreate, true)
	r.Endpoint("PUT", "/subscription/update/", p.SubscriptionUpdate, true)
}

func (r *Router) Profile(p *profile.Presenter) {
	r.Endpoint("GET", "/read/", p.Read, false)
	r.Endpoint("POST", "/create/", p.Create, true)
	r.Endpoint("PUT", "/update/", p.Update, true)
	r.Endpoint("DELETE", "/delete/", p.Delete, true)
	r.Endpoint("GET", "/code/read/", p.ReadCode, true)
	r.Endpoint("POST", "/code/create/", p.CreateCode, true)
	r.Endpoint("PUT", "/code/update/", p.UpdateCode, true)
	r.Endpoint("DELETE", "/code/delete/", p.DeleteCode, true)
}

func (r *Router) Category(p *category.Presenter) {
	r.Endpoint("GET", "/read/all/", p.ReadAll, false)
	r.Endpoint("GET", "/read/", p.Read, false)
	r.Endpoint("POST", "/create/", p.Create, true)
	r.Endpoint("PUT", "/update/", p.Update, true)
	r.Endpoint("DELETE", "/delete/", p.Delete, true)
}

func (r *Router) Product(p *product.Presenter) {
	r.Endpoint("GET", "/read/all/", p.ReadAll, false)
	r.Endpoint("GET", "/read/", p.Read, false)
	r.Endpoint("POST", "/create/", p.Create, true)
	r.Endpoint("PUT", "/update/", p.Update, true)
	r.Endpoint("DELETE", "/delete/", p.Delete, true)
}
