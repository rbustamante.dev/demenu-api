package router

import (
	"net/http"
)

type Route struct {
	Path    string
	Method  string
	Handler http.HandlerFunc
	Auth    bool
}

type Router struct {
	Mux    *http.ServeMux
	Routes []Route
	Prefix string
}

func NewRouter() *Router {
	return &Router{Mux: http.NewServeMux(), Routes: make([]Route, 0)}
}

func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	CorsMiddleware(r.Request).ServeHTTP(w, req)
}

func (r *Router) handleMatch(req *http.Request) (Route, bool) {
	for _, route := range r.Routes {
		if route.Path == req.URL.Path && route.Method == req.Method {
			return route, true
		}
	}
	return Route{}, false
}

func (r *Router) handleRoute(w http.ResponseWriter, req *http.Request, route Route) {
	if route.Auth {
		AuthMiddleware(route.Handler).ServeHTTP(w, req)
	} else {
		route.Handler.ServeHTTP(w, req)
	}
}

func (r *Router) Request(w http.ResponseWriter, req *http.Request) {
	route, found := r.handleMatch(req)
	if found {
		r.handleRoute(w, req, route)
	} else {
		http.NotFound(w, req)
	}
}

func (r *Router) Resource(source string) *Router {
	r.Mux.Handle(source+"/", http.StripPrefix(source, r.Mux))
	r.Prefix = source
	return r
}

func (r *Router) Endpoint(method string, path string, handler http.HandlerFunc, auth bool) {
	r.Routes = append(r.Routes, Route{Path: r.Prefix + path, Method: method, Handler: handler, Auth: auth})
}
