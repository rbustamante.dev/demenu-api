package router

import (
	"net/http"
	"server/auth"
	"server/helper"
)

func CorsMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", helper.GetEnv("SERVER_CORE_ORIGIN"))
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Authorization, Content-Type")
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}
		next.ServeHTTP(w, r)
	})
}

func AuthMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		cookie, err := r.Cookie("Auth-Cookie")
		if err != nil {
			http.Error(w, "Token de autorización no válido.", http.StatusUnauthorized)
			return
		}
		if !auth.ValidateToken(cookie.Value) {
			http.Error(w, "Token de autorización no válido.", http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}
